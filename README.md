# HDCore - docker-php-apache

## Introduction

This is a small container image that contains a php-apache environment with extra apache modules and mod_security.

## Image usage

## Available tags

- hdcore/docker-php-apache:8.3
- hdcore/docker-php-apache:8.4

## Deprecated tags

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-php-apache
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-php-apache

## Building image

Build:

```bash
docker compose build local-php-apache-8.3
docker compose build local-php-apache-8.4
```

## Running the image

### Default

```bash
docker compose run --rm local-php-apache-8.3
docker compose run --rm local-php-apache-8.4
```
