#!/bin/sh

# Author Danny
# Version GIT: 2021-05-25 17:09

# set-cacertificates.sh 
# Add the certificates to the local certificate store
# 
# Compatible with: centos, php

# shellcheck disable=SC2059
printf "Start set-cacertificates.sh\n"
printf "Configuring ca certificates...\n"

# Detect ca update binary
if hash "update-ca-trust" 2>/dev/null
then # CentOS/RHEL
  printf "Use update-ca-trust\n"
  certcmd="update-ca-trust"
  certpath="/etc/share/pki/ca-trust-source/anchors/"
else # Alpine
  printf "Use update-ca-certificates\n"
  certcmd="update-ca-certificates"
  certpath="/usr/local/share/ca-certificates/" 
fi 

# Create folder for certificates
printf "Create certificate folder %s\n" "${certpath}"
[ -d "${certpath}" ] || mkdir -p "${certpath}"

# Add the /certificates/ (build or run)
printf "Loop /certificates folder\n"
for file in /certificates/*.crt
do 
  if [ -e "${file}" ]
  then
    printf "Adding file %s\n" "${file}"
    cp "${file}" "${certpath}/" || exit 1
  fi
done

# Add the CACERT_FILE_* and CACERT_VAR_* from ci/cd
printf "Loop certificate variables\n"
# Remove the single quote also
set | grep "CACERT_" > /tmp/certenv.list
while IFS='=' read -r name value ; do
  value=$(eval "echo \$$name")

  case "${name}" in

    CACERT_FILE_*)
      # CACERT_FILE_*
      # 2021-05-25: the gitlab FILE type in variables has an expansion bug in scripts: issue 93089
      printf "Adding extra CA certificate file: %s\n" "${value}"
      newname=$(basename "${value}" | tr '[:upper:]' '[:lower:]')
      cp "${value}" "${certpath}${newname}.crt" || exit 11
      ;;

    CACERT_VAR_*)
      # CACERT_VAR_*
      # 2021-05-25: less secure but working as a workaround
      printf "Adding extra CA certificate value: %s\n" "${name}"
      newname=$(echo "$name" | tr '[:upper:]' '[:lower:]')
      printf "%s" "${value}" > "${certpath}${newname}.crt" || exit 12
      ;;

    *)
      ;;
  esac

done < /tmp/certenv.list
rm /tmp/certenv.list

# Import added certificates
printf "Update certificates\n"
if [ -d "${certpath}" ] && [ -n "$(ls -A "${certpath}")" ]
then
  printf "Update CA certificates\n"
  printf " You can safely ignore the skipping warning messages\n"
  $certcmd || exit 21
fi

printf "Done\n"