#!/bin/bash

# Author: Danny Herpol <hdcore@lachjekrom.com>
# Version GIT: 2022-01-02 18:21

# docker-entrypoint.sh
# for hdcore docker-php-apache image

CGREEN="\e[32m"
CNORMAL="\e[0m"

# shellcheck disable=SC2059
printf "== ${CGREEN}Start docker-entrypoint.sh ${CNORMAL} ==\n"

# Show version
printf "HDCore docker-php-apache container image\n"
printf "Installed version of php:\n"
php --version

# Adding ca-certificates
# shellcheck disable=SC1091
. set-cacertificates.sh

printf "Creating log directories"
find /etc/apache2/sites-enabled -name "*.conf" -type f -printf "%f\n" | sed 's/.conf//' | xargs -I {} mkdir -p /var/logs/apache2/{}

# shellcheck disable=SC2059
printf "== ${CGREEN}End docker-entrypoint.sh ${CNORMAL} ==\n"

# Execute docker CMD
exec "$@"
